#version 330

in vec2 textureCoordinates;
in vec3 FragPos;
in vec3 normal;

uniform vec3 meshColor;
uniform vec3 lightPosition;

uniform vec3 viewPos;

out vec4 FragColor;

void main() {

	// Normal vector normalization
	vec3 norm = normalize(normal);
	// Light direction calculation
	vec3 lightDir = normalize(lightPosition - FragPos);
	// Camera direction vector calculation
	vec3 viewDir = normalize(viewPos - FragPos);
	// Relfection direction calculation
	vec3 reflectDir = reflect(-lightDir, norm);

	// Ambient light calculation
	vec3 ambient = 0.4f * vec3(1.0f, 1.0f, 1.0f);

	// diffuse light calculation
	float diff = max(dot(norm, lightDir), 0.0);				// Calculating the diffuse intensity
	vec3 diffuse = meshColor * vec3(1.0f, 1.0f, 1.0f);		// Calculating the diffuse light power

	float spec = pow(max(dot(viewDir, reflectDir), 0.0), 1.f);			// Calculating the specular intensity
	vec3 specular = .5f * spec * vec3(1.0f, 1.0f, 1.0f);				// Calculating the specular light power

	// Summing all the different colors (ambient, specular and diffuse)
	vec3 result = (ambient * meshColor + diffuse * meshColor + specular * meshColor);

	FragColor = vec4(result, 1.0);
}