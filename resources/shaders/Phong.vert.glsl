#version 330

layout (location = 0) in vec3 a_position;
layout (location = 1) in vec3 a_normals;
layout (location = 2) in vec2 a_textureCoordinates;

uniform mat4 model;
uniform mat4 view;
uniform mat4 projection;

out vec2 textureCoordinates;
out vec3 normal;
out vec3 FragPos;

void main()
{
    // Vertex position
    gl_Position = projection * view * model * vec4(a_position, 1.0);

    // Fragment position
    FragPos = vec3(model * vec4(a_position, 1.0));

    // Texture coordinates
    textureCoordinates = a_textureCoordinates;

    // Calculating the normal
    normal = mat3(transpose(inverse(model))) * a_normals;
}
