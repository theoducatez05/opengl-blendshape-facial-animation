#version 330 core

out vec4 FragColor;

in vec2 TexCoords;

uniform vec3 meshColor;

void main()
{
	FragColor = vec4(meshColor, 1.0f);
}

