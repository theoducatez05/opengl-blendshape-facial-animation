/*****************************************************************//**
 * @file   Marker.hpp
 * @brief  The Marker header file
 *
 * @author theod
 * @date   February 2022
 *********************************************************************/

#pragma once

#include "model.hpp"
#include "shader.hpp"

#include <glm/glm.hpp>

#include <string>

/**
 * The Marker class.
 */
class Marker
{
public:

	// The model
	Model* model;

	// Position and scale of the marker
	glm::vec3 position = glm::vec3(0.0f, 0.0f, 0.0f);
	glm::vec3 scale = glm::vec3(1.0f, 1.0f, 1.0f);

	// Index of the marker and of the vertex it is attached to
	unsigned int vertexSelectedIndex;
	unsigned int indice;

	/**
	 * Constructor.
	 *
	 * \param modelPath The model file path
	 * \param vertexSelectedIndex The vertex index
	 * \param indice The marker indice
	 */
	Marker(std::string modelPath, int vertexSelectedIndex, unsigned int indice) :
		vertexSelectedIndex(vertexSelectedIndex),
		indice(indice)
	{
		model = new Model(modelPath, true);
		model->hidden = true;
	}

	~Marker()
	{
		delete model;
	}

	void Draw(Shader shader)
	{
		model->Draw(shader);
	}

	void setHidden(bool hidden)
	{
		model->hidden = hidden;
	}
};
