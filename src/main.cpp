/*****************************************************************//**
 * @file   main.cpp
 * @brief  The main program file
 *
 * Assignement 3 - Real time animation - Trinity College 2021/2022
 *
 * @author theod
 * @date   February 2022
 *********************************************************************/

// GL
#include <GL/glew.h>
#include <GL/freeglut.h>

// Eigen
#include <Eigen/Dense>
#include <Eigen/Sparse>

// glm
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtx/vector_angle.hpp>
#include <glm/gtx/string_cast.hpp>

// imgui
#include "imgui/imgui.h"
#include "imgui/imgui_impl_glut.h"
#include "imgui/imgui_impl_opengl3.h"

// includes
#include "shader.hpp"
#include "camera.hpp"
#include "model.hpp"
#include "marker.hpp"

// general includes
#include <iostream>
#include <fstream>

// settings
unsigned int SCR_WIDTH = 1920;
unsigned int SCR_HEIGHT = 1080;

// camera
Camera camera(glm::vec3(-13.0f, 17.0f, 50.0f));
glm::mat4 view;

// Keys states
bool keyPressed[256] = { false };

// Mouse position
float mousePositionX = SCR_WIDTH / 2.0f;
float mousePositionY = SCR_HEIGHT / 2.0f;

// If the player needs the mouse
bool leftMouseDown = false;

// timing
float deltaTime = 0.0;
float lastFrame = 0.0;

// Projection
glm::mat4 projection;

// Ressources
std::string RESOURCE_FOLDER = "D:/Etudes/TCD/Courses/RA/RA-Assignment_3/resources/";

// Shaders
Shader* shader;

// BlendShapes
Model* neutral;
Eigen::VectorXf neutralVertices;
Eigen::MatrixXf B;

Model* jaw_open;
Model* kiss;
Model* left_brow_lower;
Model* left_brow_narrow;
Model* left_brow_raise;
Model* left_eye_closed;
Model* left_eye_lower_open;
Model* left_eye_upper_open;
Model* left_nose_wrinkle;
Model* left_puff;
Model* left_sad;
Model* left_smile;
Model* left_suck;
Model* right_brow_lower;
Model* right_brow_narrow;
Model* right_brow_raise;
Model* right_eye_closed;
Model* right_eye_lower_open;
Model* right_eye_upper_open;
Model* right_nose_wrinkle;
Model* right_puff;
Model* right_sad;
Model* right_smile;
Model* right_suck;

Model* f;
glm::vec3 fScale = glm::vec3(1.0f, 1.0f, 1.0f);
glm::quat fOrientation = glm::angleAxis(glm::radians(-14.3f), glm::vec3(0.0f, 1.0f, 0.0f));

// Light
glm::vec3 lightPosition = glm::vec3(10.0f, 150.0f, 50.0f);

// Weights
const unsigned int WEIGHTS_NUMBER = 24;
Eigen::VectorXf weights = Eigen::VectorXf(WEIGHTS_NUMBER);
Eigen::VectorXf weightsSaved = Eigen::VectorXf(WEIGHTS_NUMBER);
std::string weightsNames[] = {
	"Jaw open",
	"Kiss",
	"Left brow lower",
	"Left brow narrow",
	"Left brow raise",
	"Left eye closed",
	"Left eye lower open",
	"Left eye upper open",
	"Left nose wrinkle",
	"Left puff",
	"Left sad",
	"Left smile",
	"Left suck",
	"Right brow lower",
	"Right brow narrow",
	"Right brow raise",
	"Right eye closed",
	"Right eye lower open",
	"Right eye upper open",
	"Right nose wrinkle",
	"Right puff",
	"Right sad",
	"Right smile",
	"Right suck"
};

// Animation
bool playAnimation = false;
bool disableSliders = false;
unsigned int currentAnimationFrame = 0;
unsigned int animationSpeed = 7;
unsigned int skippingFrames = animationSpeed;
std::vector<Eigen::VectorXf> animationWeights;

// Markers
std::vector<Marker*> markers;
bool selectedIsMarker = false;
Marker* selectedMarker;
float markerZPositionOnClick;

// Constraints
Eigen::VectorXf m = Eigen::VectorXf();
Eigen::VectorXf m0 = Eigen::VectorXf();
unsigned int currentConstraintSize = 0;

void debug(std::string s) { std::cout << s << std::endl; }

/**
 * The main UI draw function
 *
 * It draws the UI
 *
 */
void showImGui() {
	ImGui_ImplOpenGL3_NewFrame();
	ImGui_ImplGLUT_NewFrame();

	ImGui::Begin("Theo Ducatez RA 3 : Blendshapes");
	if (playAnimation){
		disableSliders = true;
		weights = animationWeights[currentAnimationFrame];
	}
	else {
		disableSliders = false;
		weights = weightsSaved;
	}
	if (disableSliders)
		ImGui::BeginDisabled();

	// Sliders
	ImGui::Dummy(ImVec2(0.0f, 5.0f));
	ImGui::Text("Eyes sliders:");
	ImGui::SliderFloat("Left brow lower", &weights(2), 0.0f, 1.0f);
	ImGui::SliderFloat("Right brow lower", &weights(13), 0.0f, 1.0f);
	ImGui::SliderFloat("Left brow narrow", &weights(3), 0.0f, 1.0f);
	ImGui::SliderFloat("Right brow narrow", &weights(14), 0.0f, 1.0f);
	ImGui::SliderFloat("Left brow raise", &weights(4), 0.0f, 1.0f);
	ImGui::SliderFloat("Right brow raise", &weights(15), 0.0f, 1.0f);
	ImGui::SliderFloat("Left eye closed", &weights(5), 0.0f, 1.0f);
	ImGui::SliderFloat("Right eye closed", &weights(16), 0.0f, 1.0f);
	ImGui::SliderFloat("Left eye lower open", &weights(6), 0.0f, 1.0f);
	ImGui::SliderFloat("Right eye lower open", &weights(17), 0.0f, 1.0f);
	ImGui::SliderFloat("Left eye upper open", &weights(7), 0.0f, 1.0f);
	ImGui::SliderFloat("Right eye upper open", &weights(18), 0.0f, 1.0f);
	ImGui::Dummy(ImVec2(0.0f, 2.0f));
	ImGui::Separator();

	ImGui::Dummy(ImVec2(0.0f, 2.0f));
	ImGui::Text("Noze sliders:");
	ImGui::SliderFloat("Left nose wrinkle", &weights(8), 0.0f, 1.0f);
	ImGui::SliderFloat("Right nose wrinkle", &weights(19), 0.0f, 1.0f);
	ImGui::Dummy(ImVec2(0.0f, 2.0f));
	ImGui::Separator();

	ImGui::Dummy(ImVec2(0.0f, 2.0f));
	ImGui::Text("Mouth sliders:");
	ImGui::SliderFloat("Jaw opened", &weights(0), 0.0f, 1.0f);
	ImGui::SliderFloat("Kiss", &weights(1), 0.0f, 1.0f);
	ImGui::SliderFloat("Left smile", &weights(11), 0.0f, 1.0f);
	ImGui::SliderFloat("Right smile", &weights(22), 0.0f, 1.0f);
	ImGui::SliderFloat("Left sad", &weights(10), 0.0f, 1.0f);
	ImGui::SliderFloat("Right sad", &weights(21), 0.0f, 1.0f);
	ImGui::Dummy(ImVec2(0.0f, 2.0f));
	ImGui::Separator();

	ImGui::Dummy(ImVec2(0.0f, 2.0f));
	ImGui::Text("Cheeks sliders:");
	ImGui::SliderFloat("Left puff", &weights(9), 0.0f, 1.0f);
	ImGui::SliderFloat("Right puff", &weights(20), 0.0f, 1.0f);
	ImGui::SliderFloat("Left suck", &weights(12), 0.0f, 1.0f);
	ImGui::SliderFloat("Right suck", &weights(23), 0.0f, 1.0f);

	// Animation and markers
	ImGui::Dummy(ImVec2(0.0f, 2.0f));
	ImGui::Separator();

	ImGui::Dummy(ImVec2(0.0f, 2.0f));
	ImGui::Text("Animation and markers:");
	if (ImGui::Button("Play animation", ImVec2(-FLT_MIN, ImGui::GetTextLineHeightWithSpacing())))
	{
		playAnimation = true;
		weightsSaved = weights;
	}
	if (ImGui::Button("Clear constraint markers", ImVec2(-FLT_MIN, ImGui::GetTextLineHeightWithSpacing())))
	{
		markers.clear();
		m0 = Eigen::VectorXf();
		m = Eigen::VectorXf();
		currentConstraintSize = 0;
		selectedIsMarker = false;
		for (unsigned int i = 0; i < WEIGHTS_NUMBER; ++i) {
			weights(i) = 0.0f;
			weightsSaved(i) = 0.0f;
		}
	}

	if (disableSliders)
	{
		ImGui::Text("Animation is playing, slider are disabled....");
		ImGui::EndDisabled();
	}

	weightsSaved = weights;

	ImGui::End();

	// Rendering
	ImGui::Render();
	ImGuiIO& io = ImGui::GetIO();
	glViewport(0, 0, (GLsizei)io.DisplaySize.x, (GLsizei)io.DisplaySize.y);
	glUseProgram(0);
	ImGui_ImplOpenGL3_RenderDrawData(ImGui::GetDrawData());
}

/**
 * @brief Glut main diplsay method (used in the main glut loop)
 *
 */
void display()
{

	glClearColor(0.7f, 0.7f, 0.9f, 1.0f);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	shader->use();

	// blendshape calculations
	Eigen::VectorXf fVertices;
	if (!playAnimation)
		fVertices = neutralVertices + B * weights;
	else
		fVertices = neutralVertices + B * animationWeights[currentAnimationFrame];

	for (unsigned int row = 0; row < neutral->getMesh().vertices.size(); ++row)
	{
		f->getMesh().vertices[row].Position.x = fVertices[3 * row];
		f->getMesh().vertices[row].Position.y = fVertices[3 * row + 1];
		f->getMesh().vertices[row].Position.z = fVertices[3 * row + 2];
	}
	f->getMesh().reloadMesh();

	// Display the face
	shader->setVec3("meshColor", glm::vec3(0.5f, 0.5f, 0.5f));
	projection = glm::perspective(glm::radians(camera.Zoom), (float)SCR_WIDTH / (float)SCR_HEIGHT, 0.1f, 100.0f);
	view = camera.GetViewMatrix();
	shader->setMat4("projection", projection);
	shader->setMat4("view", view);
	shader->setVec3("viewPosition", camera.Position);
	glm::mat4 modelMatrix = glm::mat4(1.0f);
	modelMatrix = glm::toMat4(fOrientation) * modelMatrix;
	shader->setMat4("model", modelMatrix);

	f->Draw(*shader);

	// Markers
	for (auto marker : markers)
	{
		modelMatrix = glm::mat4(1.0f);
		modelMatrix = glm::translate(modelMatrix, marker->position);
		modelMatrix = glm::scale(modelMatrix, marker->scale);
		shader->setMat4("model", modelMatrix);

		if (selectedMarker == marker)
			shader->setVec3("meshColor", glm::vec3(0.1f, 0.85f, 0.1f));
		else
			shader->setVec3("meshColor", glm::vec3(0.85f, 0.1f, 0.1f));

		marker->Draw(*shader);
	}

	// Show the UI
	showImGui();

	glutSwapBuffers();
}

/**
 * @brief Objects and shader initialization function
 *
 */
void init()
{
	// tell stb_image.h to flip loaded texture's on the y-axis (before loading model).
	stbi_set_flip_vertically_on_load(true);

	// configure global opengl state
	// -----------------------------
	glEnable(GL_DEPTH_TEST);

	// build and compile shaders
	// -------------------------
	shader = new Shader(RESOURCE_FOLDER + "shaders/Phong.vert.glsl", RESOURCE_FOLDER + "shaders/Phong.frag.glsl");
	shader->use();
	shader->setVec3("meshColor", glm::vec3(0.5f, 0.5f, 0.5f));
	shader->setVec3("lightPosition", lightPosition);

	// load blendshapes
	// -----------
	f = new Model(RESOURCE_FOLDER + "blendshapes/neutral.obj", true);

	neutral = new Model(RESOURCE_FOLDER + "blendshapes/neutral.obj", false);
	neutralVertices = Eigen::VectorXf(3 * neutral->getMesh().vertices.size());
	for (unsigned int row = 0; row < neutral->getMesh().vertices.size(); ++row)
	{
		neutralVertices(3 * row) = neutral->getMesh().vertices[row].Position.x;
		neutralVertices(3 * row + 1) = neutral->getMesh().vertices[row].Position.y;
		neutralVertices(3 * row + 2) = neutral->getMesh().vertices[row].Position.z;
	}

	jaw_open = new Model(RESOURCE_FOLDER + "blendshapes/Mery_jaw_open.obj", false);
	kiss = new Model(RESOURCE_FOLDER + "blendshapes/Mery_kiss.obj", false);
	left_brow_lower = new Model(RESOURCE_FOLDER + "blendshapes/Mery_l_brow_lower.obj", false);
	left_brow_narrow = new Model(RESOURCE_FOLDER + "blendshapes/Mery_l_brow_narrow.obj", false);
	left_brow_raise = new Model(RESOURCE_FOLDER + "blendshapes/Mery_l_brow_raise.obj", false);
	left_eye_closed = new Model(RESOURCE_FOLDER + "blendshapes/Mery_l_eye_closed.obj", false);
	left_eye_lower_open = new Model(RESOURCE_FOLDER + "blendshapes/Mery_l_eye_lower_open.obj", false);
	left_eye_upper_open = new Model(RESOURCE_FOLDER + "blendshapes/Mery_l_eye_upper_open.obj", false);
	left_nose_wrinkle = new Model(RESOURCE_FOLDER + "blendshapes/Mery_l_nose_wrinkle.obj", false);
	left_puff = new Model(RESOURCE_FOLDER + "blendshapes/Mery_l_puff.obj", false);
	left_sad = new Model(RESOURCE_FOLDER + "blendshapes/Mery_l_sad.obj", false);
	left_smile = new Model(RESOURCE_FOLDER + "blendshapes/Mery_l_smile.obj", false);
	left_suck = new Model(RESOURCE_FOLDER + "blendshapes/Mery_l_suck.obj", false);
	right_brow_lower = new Model(RESOURCE_FOLDER + "blendshapes/Mery_r_brow_lower.obj", false);
	right_brow_narrow = new Model(RESOURCE_FOLDER + "blendshapes/Mery_r_brow_narrow.obj", false);
	right_brow_raise = new Model(RESOURCE_FOLDER + "blendshapes/Mery_r_brow_raise.obj", false);
	right_eye_closed = new Model(RESOURCE_FOLDER + "blendshapes/Mery_r_eye_closed.obj", false);
	right_eye_lower_open = new Model(RESOURCE_FOLDER + "blendshapes/Mery_r_eye_lower_open.obj", false);
	right_eye_upper_open = new Model(RESOURCE_FOLDER + "blendshapes/Mery_r_eye_upper_open.obj", false);
	right_nose_wrinkle = new Model(RESOURCE_FOLDER + "blendshapes/Mery_r_nose_wrinkle.obj", false);
	right_puff = new Model(RESOURCE_FOLDER + "blendshapes/Mery_r_puff.obj", false);
	right_sad = new Model(RESOURCE_FOLDER + "blendshapes/Mery_r_sad.obj", false);
	right_smile = new Model(RESOURCE_FOLDER + "blendshapes/Mery_r_smile.obj", false);
	right_suck = new Model(RESOURCE_FOLDER + "blendshapes/Mery_r_suck.obj", false);

	//Create the delta blendshapes matrix
	// ----------

	// Calculate deltas
	std::vector<Model*> blendshapes;
	blendshapes.push_back(jaw_open);
	blendshapes.push_back(kiss);
	blendshapes.push_back(left_brow_lower);
	blendshapes.push_back(left_brow_narrow);
	blendshapes.push_back(left_brow_raise);
	blendshapes.push_back(left_eye_closed);
	blendshapes.push_back(left_eye_lower_open);
	blendshapes.push_back(left_eye_upper_open);
	blendshapes.push_back(left_nose_wrinkle);
	blendshapes.push_back(left_puff);
	blendshapes.push_back(left_sad);
	blendshapes.push_back(left_smile);
	blendshapes.push_back(left_suck);
	blendshapes.push_back(right_brow_lower);
	blendshapes.push_back(right_brow_narrow);
	blendshapes.push_back(right_brow_raise);
	blendshapes.push_back(right_eye_closed);
	blendshapes.push_back(right_eye_lower_open);
	blendshapes.push_back(right_eye_upper_open);
	blendshapes.push_back(right_nose_wrinkle);
	blendshapes.push_back(right_puff);
	blendshapes.push_back(right_sad);
	blendshapes.push_back(right_smile);
	blendshapes.push_back(right_suck);

	for (Model* blendshape : blendshapes)
	{
		blendshape->substractNeutral(neutral->getMesh().vertices);
	}

	// Creating the B matrix
	B = Eigen::MatrixXf(3 * neutral->getMesh().vertices.size(), WEIGHTS_NUMBER);
	for (unsigned int column = 0; column < WEIGHTS_NUMBER; ++column) {
		for (unsigned int row = 0; row < neutral->getMesh().vertices.size(); ++row)
		{
			B(3 * row, column) = blendshapes[column]->getMesh().vertices[row].Position.x;
			B(3 * row + 1, column) = blendshapes[column]->getMesh().vertices[row].Position.y;
			B(3 * row + 2, column) = blendshapes[column]->getMesh().vertices[row].Position.z;
		}
	}

	// Initialize weights
	// ----------
	for (unsigned int i = 0; i < WEIGHTS_NUMBER; ++i) {
		weights(i) = 0.0f;
		weightsSaved(i) = 0.0f;
	}

	// Init animation
	// ----------

	// Parse the file to get the weights
	animationWeights = std::vector<Eigen::VectorXf>(250, Eigen::VectorXf(WEIGHTS_NUMBER));

	std::fstream fileStream;
	fileStream.open(RESOURCE_FOLDER + "animation/animation.txt");
	std::string line;

	for (unsigned int i = 0; i < 250 ; ++i)
	{
		std::getline(fileStream, line);
		std::stringstream  lineStream(line);

		Eigen::VectorXf fileWeightVector(WEIGHTS_NUMBER);
		for (unsigned int y = 0; y < WEIGHTS_NUMBER	; ++y)
		{
			float a;
			lineStream >> a;
			animationWeights[i](y) = (float)a;
		}
	}
}

/**
 * @brief Process a window resize event
 *
 * @param width The new window width
 * @param height The new window height
 */
void resizeWindow(int width, int height)
{
	mousePositionX = width / 2.0f;
	mousePositionY = height / 2.0f;

	SCR_HEIGHT = height;
	SCR_WIDTH = width;

	glViewport(0, 0, width, height); // reset the viewport
	projection = glm::perspective(glm::radians(camera.Zoom), (float)SCR_WIDTH / (float)SCR_HEIGHT, 0.1f, 100.0f);

	ImGui_ImplGLUT_ReshapeFunc(width, height);
}

/**
 * @brief When a key is pressed but doesn't need real time reaction
 *
 * @param key The key
 */
void keyPress(unsigned char key, int x, int y)
{
	keyPressed[key] = true;

	ImGui_ImplGLUT_KeyboardFunc(key, x, y);
	glutPostRedisplay();
}

void processKeyInputs()
{
	// Camera movements
	if (keyPressed['z'])
		camera.ProcessKeyboard(CameraMovement::FORWARD, 3.0f * deltaTime);
	if (keyPressed['s'])
		camera.ProcessKeyboard(CameraMovement::BACKWARD, 3.0f * deltaTime);
	if (keyPressed['q'])
		camera.ProcessKeyboard(CameraMovement::LEFT, 3.0f * deltaTime);
	if (keyPressed['d'])
		camera.ProcessKeyboard(CameraMovement::RIGHT, 3.0f * deltaTime);
}

/**
* @brief When a key is released
*
* @param key The key
*/
void keyUp(unsigned char key, int x, int y) {

	keyPressed[key] = false;

	ImGui_ImplGLUT_KeyboardUpFunc(key, x, y);
	glutPostRedisplay();
}

/**
 * @brief Glut idle function callback, to update the scene.
 *
 */
void updateScene()
{

	// per-frame time logic
	float currentFrame = (float)glutGet(GLUT_ELAPSED_TIME) * 0.001f;
	deltaTime = (currentFrame - lastFrame);
	lastFrame = currentFrame;

	// Update animation frame
	if (playAnimation) {
		skippingFrames--;
		if (skippingFrames == 0) {
			skippingFrames = animationSpeed;
			currentAnimationFrame += 1;
			if (currentAnimationFrame >= 250) {
				currentAnimationFrame = 0;
				playAnimation = false;
			}
		}
	}

	// Process key inputs
	processKeyInputs();

	// Draw the next frame
	glutPostRedisplay();
}


/**
 * Get the position in 3D where at 2D x and y positions.
 *
 * \param x X coordinate
 * \param y Y coordinate
 * \return The 3D position of this 2D on screen position
 */
glm::vec3 getObject(int x, int y)
{
	glm::vec3 window;
	window.x = (float)x;
	window.y = SCR_HEIGHT - (float)y - 1;
	glReadPixels(x, SCR_HEIGHT - y - 1, 1, 1, GL_DEPTH_COMPONENT, GL_FLOAT, &window.z);

	// get object corrdinate
	return glm::unProject(window, view, projection, glm::vec4(0.0f, 0.0f, SCR_WIDTH, SCR_HEIGHT));
}

/**
 * @brief Mouse movement callback
 *
 * @param x X position
 * @param y Y position
 */
void mouseMove(int x, int y)
{
	if (x > 0) {
		if (selectedIsMarker) {
			glm::vec3 position = getObject(x, y);
			position.z = markerZPositionOnClick;
			selectedMarker->position = position;
		}
	}
	ImGui_ImplGLUT_MotionFunc(x, y);
}

void mouseInputs(int button, int state, int x, int y)
{
	if (button == GLUT_LEFT_BUTTON)
	{
		if (state == GLUT_DOWN)
		{
			leftMouseDown = true;

			// get object corrdinate
			glm::vec3 object = getObject(x, y);

			// brute force
			float minimum = 10.0f;
			float temp;
			unsigned int indice = 0;
			unsigned int markerIndice = 0;
			for (unsigned int i = 0; i < f->getMesh().vertices.size(); ++i)
			{
				temp = glm::distance(object, f->getMesh().vertices[i].Position);
				if (temp <= minimum)
				{
					minimum = temp;
					indice = i;
					selectedIsMarker = false;
				}
			}
			for (unsigned int y = 0; y < markers.size(); y++)
			{
				temp = glm::distance(object, markers[y]->position);
				debug(std::to_string(temp));
				if (temp <= minimum || temp <= 0.80f)
				{
					minimum = temp;
					selectedMarker = markers[y];
					selectedIsMarker = true;
					markerIndice = y;
				}
			}

			if (!selectedIsMarker)
			{
				Marker* marker = new Marker(RESOURCE_FOLDER + "sphere.obj", indice, currentConstraintSize);
				marker->position = f->getMesh().vertices[indice].Position;
				marker->setHidden(false);
				markers.push_back(marker);
				m.conservativeResize(currentConstraintSize * 3 + 3);
				m0.conservativeResize(currentConstraintSize * 3 + 3);
				currentConstraintSize++;
				selectedMarker = marker;
			}
			else {
				markerZPositionOnClick = f->getMesh().vertices[indice].Position.z;
				m0(3 * markerIndice) = selectedMarker->position.x;
				m0(3 * markerIndice + 1) = selectedMarker->position.y;
				m0(3 * markerIndice + 2) = selectedMarker->position.z;
			}
		}
		else if (state == GLUT_UP)
		{
			leftMouseDown = false;
			if (selectedIsMarker) {
				m(3 * selectedMarker->indice) = selectedMarker->position.x;
				m(3 * selectedMarker->indice + 1) = selectedMarker->position.y;
				m(3 * selectedMarker->indice + 2) = selectedMarker->position.z;

				// Make B
				Eigen::MatrixXf B2 = Eigen::MatrixXf(currentConstraintSize * 3, WEIGHTS_NUMBER);
				for (int i = 0; i < markers.size(); ++i)
				{
					for (int y = 0; y < WEIGHTS_NUMBER; ++y)
					{
						B2(i * 3, y) = B(markers[i]->vertexSelectedIndex * 3, y);
						B2(i * 3 + 1, y) = B(markers[i]->vertexSelectedIndex * 3 + 1, y);
						B2(i * 3 + 2, y) = B(markers[i]->vertexSelectedIndex * 3 + 2, y);
					}
				}

				Eigen::MatrixXf A = Eigen::MatrixXf(WEIGHTS_NUMBER, WEIGHTS_NUMBER);
				Eigen::VectorXf b = Eigen::VectorXf(currentConstraintSize * 3);
				float alpha = 1;

				// Make A and b
				A = B2.transpose() * B2 + (alpha + 0.0001) * Eigen::Matrix<float, WEIGHTS_NUMBER, WEIGHTS_NUMBER>::Identity();
				b = B2.transpose() * (m - m0) + alpha * weights;

				Eigen::LDLT<Eigen::MatrixXf> solver(A);
				Eigen::VectorXf w = solver.solve(b);

				for (float& weightOfw : w) {
					if (weightOfw > 1.0f)
						weightOfw = 1.0f;
					if (weightOfw < 0.0f)
						weightOfw = 0.0f;
				}

				weights = w;
				weightsSaved = w;
			}
		}
	}

	ImGui_ImplGLUT_MouseFunc(button, state, x, y);
	glutPostRedisplay();
}

/**
 * @brief Main function
 *
 * @param argc System arguments count
 * @param argv System arguments
 */
int main(int argc, char** argv)
{
	// Set up the window
	glutInit(&argc, argv);
	glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGB);
	glutInitWindowSize(SCR_WIDTH, SCR_HEIGHT);
	glutCreateWindow("CS7GV5 | Assignment 3 | Theo Ducatez");

	GLenum res = glewInit();

	// Check for any errors
	if (res != GLEW_OK) {
		std::cout << "Error: \n" << glewGetErrorString(res) << std::endl;
		return 1;
	}

	// Set up input configurations and callbacks
	glutKeyboardFunc(keyPress);
	glutKeyboardUpFunc(keyUp);
	glutMotionFunc(mouseMove);
	glutMouseFunc(mouseInputs);

	// Register general call back funtions
	glutDisplayFunc(display);
	glutIdleFunc(updateScene);
	glutReshapeFunc(resizeWindow);

	// Setup Dear ImGui context
	IMGUI_CHECKVERSION();
	ImGui::CreateContext();
	ImGuiIO& io = ImGui::GetIO(); (void)io;

	// Setup Dear ImGui style
	ImGui::StyleColorsDark();

	// Setup Platform/Renderer backends
	ImGui_ImplGLUT_Init();

	glutPassiveMotionFunc(ImGui_ImplGLUT_MotionFunc);
#ifdef __FREEGLUT_EXT_H__
	glutMouseWheelFunc(ImGui_ImplGLUT_MouseWheelFunc);
#endif
	glutSpecialFunc(ImGui_ImplGLUT_SpecialFunc);
	glutSpecialUpFunc(ImGui_ImplGLUT_SpecialUpFunc);
	ImGui_ImplOpenGL3_Init("#version 330");

	// Setting up the graphic objects
	init();

	// Begin infinite event loop
	glutMainLoop();

	// Cleanup
	ImGui_ImplOpenGL3_Shutdown();
	ImGui_ImplGLUT_Shutdown();
	ImGui::DestroyContext();

	return 0;
}

